/// <reference types="Cypress" />

context('Homepage', () => {

  it('Nav menu', () => {
    cy.visit('http://localhost:8080')
    cy.get('.navbar-nav').contains('Commands').click()
    cy.get('.dropdown-menu').should('contain','Navigation')
  })
})
